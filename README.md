# Extract pour la meteo

**IMPORTANT: Le VPN de PROD est indispensable pour pouvoir executer ces scripts**

## Installation
### Installer PIP:
- Telecharger _get-pip.py_: https://bootstrap.pypa.io/get-pip.py
- `python get-pip.py`
- Se placer dans ce dossier et lancer `pip install -r requirements.txt`


## Pour lancer la météo
- Se placer dans le dossier et lancer `jupyter notebook`
- Lancer le notebook *extract_eric_sen*
- Cliquer sur **Kernel > Restart and run all**
- Le csv sera dans le dossier `./data`

## Pour utiliser le résultat de ton TCD avec le test bayésien
- Lancer le notebook *bayesian_abtest*
- Modifier les champs *visitor_A, visitor_B, sales_A* et *sales_B*
- Cliquer sur **Kernel > Restart and run all**

## Pour passer en mode Dashboard
- Une fois sur le notebook cliquer sur **View > Dashboard preview**
